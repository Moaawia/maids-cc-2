import { Component } from '@angular/core';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'maids-cc-2';
  constructor(public loadingService: LoadingService) {}
  isLoading = false;

  makeNetworkRequest() {
    this.isLoading = true;

    
    setTimeout(() => {
      this.isLoading = false;
    
    }, 3000); 
  }
}
